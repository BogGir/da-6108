#pragma once

#include <string>
//#include <fstream>
#include <iostream>

#include "Song.h"
#include "JSON.h"
#include "JSONValue.h"



class JSONFile
{
public:
	static Song* readSongFromFile(const char* file_name);

	static std::vector<Song*> readVectorSongFromFile(const char* file_name);

private:
	static JSONValue* openJSONFile(const char* file_name);
	static Song* readJSONSong(JSONObject root);
	
	static std::string wsTOs(const std::wstring ws);
	static std::string wcTOs(const wchar_t* wc);
	static std::wstring sTOws(const std::string s);
	static std::wstring cTOws(const char* c);
	
	static std::string JSONFile::GetStringField(JSONObject root, std::string field_Name);
	static double GetNumberField(JSONObject root, std::string field_Name);
};