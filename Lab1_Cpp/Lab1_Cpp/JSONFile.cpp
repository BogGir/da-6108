#include "JSONFile.h"

#include <string>
#include <vector>
#include <fstream>
#include <iterator>
#include <iostream>

#include "Song.h"
#include "JSON.h"
#include "JSONValue.h"



std::vector<Song*> JSONFile::readVectorSongFromFile(const char* file_name)
{
	JSONValue *pV = openJSONFile(file_name);
	if (NULL != pV)
	{
		JSONObject root = pV->AsObject();

		std::vector <Song*> vectorSongs;
		if (root.find(L"array_songs") != root.end() && root[L"array_songs"]->IsArray())
		{
			JSONArray array = root[L"array_songs"]->AsArray();

			for (unsigned int i = 0; i < array.size(); i++)
			{
				JSONObject tmp = array[i]->AsObject();

				Song *s = readJSONSong(tmp);
				vectorSongs.insert(vectorSongs.end(), s);
			}
		}
		return vectorSongs;
	}
}

JSONValue* JSONFile::openJSONFile(const char* file_name)
{
	std::ifstream file(file_name);
	// check if filename is file
	if (file.is_open())
	{
		std::string str, tmp;

		while (getline(file, tmp))
		{
			str = str + tmp;
		}

		JSONValue *pV = JSON::Parse(str.c_str());
		if (pV != NULL)
			return pV;
	}
	return NULL;
}
	
Song* JSONFile::readJSONSong(JSONObject root)
{
	Song *s = new Song;

	if (NULL != s)
	{
		s->SetName(GetStringField(root, "name"));
		s->SetAuthor(GetStringField(root, "author"));
		s->SetLength(GetNumberField(root, "length"));
		s->SetSize(GetNumberField(root, "size"));
		s->SetAlbum(GetStringField(root, "album"));
		s->SetGanre(GetStringField(root, "ganre"));
		s->SetYear(GetStringField(root, "year"));
	}

	return s;
}

Song* JSONFile::readSongFromFile(const char* file_name)
{
	JSONValue *pV = openJSONFile(file_name);
	if (NULL != pV)
	{
		JSONObject root;

		//check if root is OK
		root = pV->AsObject();

		//create song from JSON file
		Song *s = readJSONSong(root);

		return s;
	}
	return NULL;
}

std::string JSONFile::wsTOs(const std::wstring ws)
{
	return std::string(ws.begin(), ws.end());
}
std::string JSONFile::wcTOs(const wchar_t* wc)
{
	std::wstring ws(wc);
	return std::string(ws.begin(), ws.end());
}
std::wstring JSONFile::sTOws(const std::string s)
{
	return std::wstring(s.begin(), s.end());
}
std::wstring JSONFile::cTOws(const char* c)
{
	std::string s(c);
	return std::wstring(s.begin(), s.end());
}

std::string JSONFile::GetStringField(JSONObject root, std::string field_Name)
{
	std::string res = "";
	const std::wstring key = sTOws(field_Name);
	if (root.find(key) != root.end() && root[key]->IsString())
	{
		const wchar_t *wc = root[key]->AsString().c_str();
		res = wcTOs(wc);
	}
	return res;
}
	
double JSONFile::GetNumberField(JSONObject root, std::string field_Name)
{
	double res = 0;
	const std::wstring key = sTOws(field_Name);

	if (root.find(key) != root.end() && root[key]->IsNumber())
	{
		res = root[key]->AsNumber();
	}
	return res;
}	