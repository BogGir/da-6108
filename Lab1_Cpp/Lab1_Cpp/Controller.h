#pragma once

#include "View.h"
#include "Model.h"



class Controller
{
private:
	Model model;
	View view;

	static const std::string Controller::WAY;


public:
	Controller();

	Controller::Controller(Model model, View view);

	int Controller::ProcessUser();
	
	std::string Controller::TakeWayToFile();
	std::string Controller::TakeKeyToSort();
	
	bool Controller::ExistsTest(const std::string & way);
	bool Controller::TakeAnswer(std::string mess);
	bool Controller::AskToCollection(std::string ask);

	
	~Controller();
};

