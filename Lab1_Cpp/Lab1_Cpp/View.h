#pragma once

#include <string>
#include <vector>

#include "Song.h"



class View
{
	
public:
	static const std::string View::ERROR;

	static const std::string View::LIST_OF_SONGS;
	static const std::string View::NUMBER_OF_SONGS;
	static const std::string View::ENTER_WAY_TO_FILE;
	static const std::string View::NOT_VALID_ADRESS;
	static const std::string View::TRUE_WAY;
	static const std::string View::NEW_LINE;
	
	static const std::string View::NAME;
	static const std::string View::AUTHOR;
	static const std::string View::ALBUM;
	static const std::string View::GANRE;
	static const std::string View::YEAR;
	static const std::string View::LENGTH;
	static const std::string View::SIZE;
    
	static const std::string View::ASK_TO_COLLECTION;
	static const std::string View::BYE;
	static const std::string View::ENTER_KEY;
	static const std::string View::NOT_VALID_KEY;
	static const std::string View::CREATE_NEW_COLLECTION;
	static const std::string View::ADD_THIS;
	static const std::string View::ADD_MORE;

	static const std::string View::MUSIC_LIBRARY;
	static const std::string View::AMOUNT_SONGS;
	static const std::string View::AUTHORS;
	static const std::string View::GANRES;
	static const std::string View::DURATION_SONGS;

	static const std::string View::YOUR_COLLECTION;

	static const std::string View::Exit;





	//static const std::string View::

	static void View::PrintMessage(std::string massage);
	static void View::CoutListSongs(std::vector <Song*> vectorSong);
	static void View::PrintMesAndStr(std::string mes, std::string str);
	static void View::PrintMesAndInt(std::string mes, int value);
	static void View::PrintNewLineAndMessage(std::string str);
	static void View::PrintEmptyLine();
	std::string TakeWay();

	static void View::PrintVectorString(std::vector<std::string> &vectorStrings);



	View();
	~View();
};

