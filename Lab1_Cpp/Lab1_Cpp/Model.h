#pragma once

#include <string>
#include <fstream>

#include "View.h"
#include "JSONValue.h"
#include "JSONFile.h"
#include "Collection.h"



class Model
{

private:
	std::vector <Song*>		   SongsVector;
	std::vector <std::string> vectorAuthors;
	std::vector <std::string> vectorGanres;
	//std::vector <std::string> vectorAuthors;

	//std::vector<Song> CollectionSong;
	std::string Model::TakeField(Song * song, std::string nameField);
	
	std::vector <std::string> Model::TakeVectorFields(std::vector <Song*> vectorSongs, std::string nameField);

	bool Model::CheckVector(std::vector <std::string> vectorFields, std::string valueField);
	
	

public:

	static const std::string Model::NAME;
	static const std::string Model::GANRE;
	static const std::string Model::AUTHOR;

	static const std::string Model::EMPTY_FIELD;


	Model();
	
	std::vector	<Song*> Model::GetSongsVector();
	std::vector <std::string> Model::TakeAuthors(std::vector <Song*> vectorSongs);
	std::vector <std::string> Model::TakeGanres(std::vector <Song*> vectorSongs);

	int Model::CountAmountSongs(std::vector <Song*> vecorSongs);
	int Model::CountDurationSongs(std::vector <Song*> vectorSongs);

	void Model::CreateVectorSongs(std::string way);
	void Model::CreateVectorAuthors(std::vector <Song*> vecorSongs);
	void Model::CreateVectorGanres(std::vector <Song*> vecorSongs);

	std::vector<std::string> GetVectorAuthors();
	std::vector<std::string> GetVectorGanres();

	~Model();
};