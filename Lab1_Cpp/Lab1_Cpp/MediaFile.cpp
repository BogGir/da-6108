#include<string>
#include "MediaFile.h"

MediaFile::MediaFile()
{
	name = author = "";
	length = size = 0;
}

MediaFile::MediaFile(std::string name, std::string author, int length, int size)
{
	MediaFile::SetAll(name, author, length, size);
}

void MediaFile::SetAll(std::string name, std::string author, int length, int size)
{
	SetName(name);
	SetAuthor(author);
	SetLength(length);
	SetSize(size);
}

void MediaFile::SetName(std::string name)
{
	this->name = name;
}

void MediaFile::SetAuthor(std::string author)
{
	this->author = author;
}

void MediaFile::SetLength(int length)
{
	if (length >= 0)
	{
		this->length = length;
	}
}

void MediaFile::SetSize(int size)
{
	if (length >= 0)
	{
		this->size = size;
	}
}

std::string MediaFile::GetName()
{
	return name;
}

std::string MediaFile::GetAuthor()
{
	return author;
}

int MediaFile::GetLength()
{
	return length;
}

int MediaFile::GetSize()
{
	return size;
}