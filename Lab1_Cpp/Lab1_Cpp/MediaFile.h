#pragma once
#include <string>



class MediaFile
	{
	protected:
		
		std::string name;
		std::string author;
		int length;
		int size;

	public:
		MediaFile::MediaFile();

		MediaFile::MediaFile(std::string name, std::string author, int length, int size);

		void MediaFile::SetAll(std::string name, std::string author, int length, int size);

		void MediaFile::SetName(std::string name);

		void MediaFile::SetAuthor(std::string author);
		
		void MediaFile::SetLength(int length);

		void MediaFile::SetSize(int size);

		std::string MediaFile::GetName();

		std::string MediaFile::GetAuthor();

		int MediaFile::GetLength();

		int MediaFile::GetSize();

		//virtual void MediaFile::VirtualFunction() = 0;
};

