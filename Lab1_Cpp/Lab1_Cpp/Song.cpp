#include <string>
#include "Song.h"
#include "MediaFile.h"

Song::Song():MediaFile::MediaFile(){
	Song::album = Song::author = Song::ganre = "";
}

Song::Song(std::string name, std::string album, std::string author, std::string year, std::string ganre, int length, int size)
	:MediaFile::MediaFile(name, author, length, size)
{
	SetAlbum(album);
	SetGanre(ganre);
	SetYear(year);
}

void Song::SetAll(std::string name, std::string album, std::string author, std::string year, std::string ganre , int length, int size)
{
	MediaFile::SetAll(name, author, length, size);
	SetAlbum(album);
	SetGanre(ganre);
	SetYear(year);
}

void Song::SetAlbum(std::string album)
{
	this->album = album;
}

void Song::SetGanre(std::string ganre)
{
	this->ganre = ganre;
}

void Song::SetYear(std::string year)
{
	this->year = year;
}

std::string Song::GetAlbum()
{
	return album;
}

std::string Song::GetGanre()
{
	return ganre;
}

std::string Song::GetYear()
{
	return year;
}

Song::~Song()
{
}
