#pragma once
#include <vector>
#include <iterator>
#include "Song.h"
#include "MediaFile.h"

class Collection : public MediaFile
{
private:
	std::vector <Song> Collection::songCollection;
	std::vector <Song> Collection::songVector;

	/*enum Options
	{
		Name,
		Author,
		Ganre,
		Year,
	    Length,
		Size
	};
	*/
	
public:
	Collection::Collection();
	Collection::Collection(std::string name, std::string author, int length, int size, std::vector <Song> song_tmp);
	void Collection::SetVectorSongs(std::vector <Song> song_tmp);
	void Collection::SortVector(std::vector<Song> songVec, std::string key);
	void Collection::AddSong(Song *pSong);
	Collection::~Collection();
};

