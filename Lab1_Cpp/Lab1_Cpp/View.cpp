#include "View.h"

#include <string>
#include <iostream>



const std::string View::LIST_OF_SONGS = "List of songs:";
const std::string View::ENTER_WAY_TO_FILE = "Enter way to file with songs:";
const std::string View::NOT_VALID_ADRESS = "File not faund!";
const std::string View::NUMBER_OF_SONGS = "Number songs in list:";
const std::string View::TRUE_WAY = "File is found.";
const std::string View::NEW_LINE = "\n";

const std::string View::NAME   = ". Name: ";
const std::string View::AUTHOR = ", Author: ";
const std::string View::ALBUM  = ", Album: ";
const std::string View::GANRE  = ", Ganre: ";
const std::string View::YEAR   = ", Year: ";
const std::string View::LENGTH = ", Length: ";
const std::string View::SIZE   = ", Size: ";

const std::string View::MUSIC_LIBRARY = "Music library contains: ";
const std::string View::AMOUNT_SONGS = "Amount songs: ";
const std::string View::AUTHORS = "Authors: ";
const std::string View::GANRES = "Ganres in library: ";
const std::string View::DURATION_SONGS = "Durations all songs: ";

const std::string View::ASK_TO_COLLECTION = "Do you want create collection?\n Enter Y if Yes or N if No .\nAnswer:";

const std::string View::Exit = "Program is finished. Bye.";



//const std::string View::ASKTOCOLLECT = "Do you want create collectiion?\nIf yes enter 1\n Else enter 2";
//const std::string View::CREAT_COLLECTION = "";

void View::PrintMessage(std::string massage)
{
	std::cout << massage;
}


void View::CoutListSongs(std::vector <Song*> vectorSong)
{
	for (int i = 0; i < vectorSong.size(); i++)
	{
		View::PrintMesAndInt("\n", i + 1);
		View::PrintMesAndStr(View::NAME, vectorSong[i]->GetName());
		View::PrintMesAndStr(View::AUTHOR, vectorSong[i]->GetAuthor());
		View::PrintMesAndStr(View::ALBUM, vectorSong[i]->GetAlbum());
		View::PrintMesAndStr(View::GANRE, vectorSong[i]->GetGanre());
		View::PrintMesAndStr(View::YEAR, vectorSong[i]->GetYear());
		View::PrintMesAndInt(View::LENGTH, vectorSong[i]->GetLength());
		View::PrintMesAndInt(View::SIZE, vectorSong[i]->GetSize());
		View::PrintMessage(View::NEW_LINE);
	}
	View::PrintMessage(View::NEW_LINE);
}

void View::PrintMesAndStr(std::string mes, std::string str)
{
	std::cout << mes << str;
}

void View::PrintMesAndInt(std::string mes, int value)
{
	std::cout << mes << value;
}

void View::PrintNewLineAndMessage(std::string str)
{
	std::cout << "\n" << str<<std::endl;
}

void View::PrintEmptyLine()
{
	std::cout << std::endl;
}



std::string View::TakeWay()
{
	return std::string();
}

void View::PrintVectorString(std::vector<std::string> &vectorStrings)
{
	std::cout << std::endl;
	for (int i = 0; i < vectorStrings.size(); i++)
	{
		std::cout << i+1 << "." << vectorStrings[i] << std::endl;
	}
}


View::View()
{
}


View::~View()
{
}
