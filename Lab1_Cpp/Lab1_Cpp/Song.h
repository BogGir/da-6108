#pragma once

#include <string>

#include"MediaFile.h"



class Song : public MediaFile
{
private:
	std::string album;
	std::string ganre;
	std::string year;
	
public:

	Song::Song();

	Song::Song(std::string name, std::string album, std::string author, std::string year, std::string ganre, int length, int size);
	
	void Song::SetAll(std::string name, std::string album, std::string author, std::string year, std::string ganre, int length, int size);

	void Song::SetAlbum(std::string album);

	void Song::SetGanre(std::string ganre);

	void Song::SetYear(std::string year);

	std::string Song::GetAlbum();
	
	std::string Song::GetGanre();
	
	std::string Song::GetYear();
	
	Song::~Song();
};