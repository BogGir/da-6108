#define _CRT_SECURE_NO_WARNINGS
#include "Model.h"

#include <string>
#include <fstream>
#include <streambuf>
#include <cstdio>
#include <vector>

#include "MediaFile.h"
#include "Song.h"
#include "JSON.h"
#include "View.h"
#include "JSONValue.h"
#include "JSONFile.h"

const std::string Model::NAME   = "name";
const std::string Model::GANRE  = "ganre";
const std::string Model::AUTHOR = "author";
const std::string Model::EMPTY_FIELD = "-";


// constructor
Model::Model()
{
}

// checks vector for repetition fields
bool Model::CheckVector(std::vector <std::string> vectorFields, std::string valueField)
{
	bool flag;
	
	if (valueField == ""|| valueField == Model::EMPTY_FIELD)
		return false;

	if (vectorFields.size() == 0)
		return true;

	for(unsigned int i = 0; i < vectorFields.size(); i++)
	{
	
		if ((valueField).compare(vectorFields[i]) == 0)
		{
			return false;
		}
	}

	return true;
}

std::string Model::TakeField(Song * song, std::string nameField)
{
	std::string valueField;
	

	if (nameField == Model::NAME)   valueField = song->GetName();
	if (nameField == Model::GANRE)  valueField = song->GetGanre();
	if (nameField == Model::AUTHOR) valueField = song->GetAuthor();
	
	return valueField;
}

std::vector<std::string> Model::TakeVectorFields(std::vector<Song*> vectorSongs, std::string nameField)
{
	std::vector <std::string> vectorFields;
	
	for (unsigned i = 0 ; i < vectorSongs.size(); i++)
	{
		std::string tmp_str = Model::TakeField(vectorSongs[i], nameField);
	
		if (Model::CheckVector(vectorFields, tmp_str) == true)
		{
			vectorFields.push_back(tmp_str);
		}
	}
	
	
	return vectorFields;
}

int Model::CountAmountSongs(std::vector<Song*> vectorSongs)
{
	int amountSongs = vectorSongs.size();

	return amountSongs;
}

int Model::CountDurationSongs(std::vector<Song*> vectorSongs)
{
	int duration = 0;

	int size = vectorSongs.size();

	for (int i = 0; i < size; i++)
	{
		duration += vectorSongs[i]->GetLength();
	}

	return duration;
}

void Model::CreateVectorSongs(std::string way)
{
	SongsVector = JSONFile::readVectorSongFromFile(way.c_str());
}

void Model::CreateVectorAuthors(std::vector <Song*> vectorSongs)
{
	vectorAuthors = Model::TakeVectorFields(vectorSongs, Model::AUTHOR);
}

void Model::CreateVectorGanres(std::vector <Song*> vectorSongs)
{
	vectorGanres = Model::TakeVectorFields(vectorSongs, Model::GANRE);
}

std::vector<std::string> Model::GetVectorAuthors()
{
	return vectorAuthors;
}

std::vector<std::string> Model::GetVectorGanres()
{
	return vectorGanres;
}

std::vector<Song*> Model::GetSongsVector()
{
	return SongsVector;
}

std::vector<std::string> Model::TakeAuthors(std::vector<Song*> vectorSongs)
{
	std::vector <std::string> vectorTmp = Model::TakeVectorFields(vectorSongs, Model::AUTHOR);

	return vectorTmp;
}

std::vector<std::string> Model::TakeGanres(std::vector<Song*> vectorSongs)
{
	std::vector <std::string> vectorGanres = Model::TakeVectorFields(vectorSongs, Model::GANRE);

	return vectorGanres;
}

Model::~Model()
{
}