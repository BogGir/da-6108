#include "Model.h"
#include "View.h"
#include "Controller.h"

int main()
{
	// Initialization
	Model model;
	View view;
	Controller controller(model, view);
	
	//run
	controller.ProcessUser();

	return 0;
}