#include "Controller.h"
#include "Model.h"
#include "View.h"

const std::string Controller::WAY = "d:\\test_file.json";

int Controller::ProcessUser()
{

	//std::string way = "d:\test_file.json";
	//model.CreateVectorSongs(Controller::TakeWayToFile());

	model.CreateVectorSongs(Controller::WAY);

	view.CoutListSongs(model.GetSongsVector());

	view.PrintNewLineAndMessage(view.MUSIC_LIBRARY);
	view.PrintEmptyLine();
	view.PrintMesAndInt(view.AMOUNT_SONGS, model.CountAmountSongs(model.GetSongsVector()));

	view.PrintMessage(view.NEW_LINE);
	view.PrintNewLineAndMessage(view.AUTHORS);

	//create vectors authors and ganres
	model.CreateVectorAuthors(model.GetSongsVector());
	model.CreateVectorGanres(model.GetSongsVector());

	// cout this vectors 
	view.PrintVectorString(model.GetVectorAuthors());
	view.PrintMessage(view.NEW_LINE);

	// count  
	view.PrintMesAndInt(view.DURATION_SONGS, model.CountDurationSongs(model.GetSongsVector()));

	if (!AskToCollection(view.ASK_TO_COLLECTION))
		return 0;


}
std::string Controller::TakeWayToFile()
{
	std::string way;
	view.PrintMessage(view.ENTER_WAY_TO_FILE);
	std::cin >> way;
	while(Controller::ExistsTest(way) == false)
	{
		view.PrintMesAndStr(view.NEW_LINE, view.NOT_VALID_ADRESS);
		view.PrintMesAndStr(view.NEW_LINE, view.ENTER_WAY_TO_FILE);

 		std::cin >> way;
	}
	view.PrintMesAndStr(view.NEW_LINE, view.TRUE_WAY);
	view.PrintMessage(view.NEW_LINE);
	return way;
}

std::string Controller::TakeKeyToSort()
{
	std::string key;
//	view.PrintMessage();

	return key;
}

bool Controller::ExistsTest(const std::string& way)
{
	std::ifstream f(way.c_str());
	return f.good();
}

bool Controller::TakeAnswer(std::string mess)
{
	std::string answer;
	view.PrintNewLineAndMessage(mess);
	std::cin >> answer;
	if (answer == "Y")
		return true;
	if (answer == "N")
		return false;
}

bool Controller::AskToCollection(std::string ask)
{
	if (!TakeAnswer(ask))
	{
		view.PrintEmptyLine();
		view.PrintNewLineAndMessage(view.Exit);
		
		return false;
	}
	else
	{
		//method for create collection
	
		return true;
	}
}

Controller::Controller(Model model_tmp, View view_tmp)
{
	model = model_tmp;
	view = view_tmp;
}

Controller::Controller()
{
}

Controller::~Controller()
{
}
