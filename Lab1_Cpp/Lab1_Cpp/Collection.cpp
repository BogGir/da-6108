#include <string>
#include <vector>
#include <algorithm>
//#include <stdio.h>  
//#include <iostream>

#include "Song.h"
#include "MediaFile.h"
#include "Collection.h"
#include "View.h"

bool compareOnName   (Song song1, Song song2) { return  (song1.GetName()   < song2.GetName()   ); }
bool compareOnAuthor (Song song1, Song song2) { return  (song1.GetAuthor() < song2.GetAuthor() ); }
bool compareOnAlbum  (Song song1, Song song2) { return  (song1.GetAlbum()  < song2.GetAlbum()  ); }
bool compareOnGanre  (Song song1, Song song2) { return  (song1.GetGanre()  < song2.GetGanre()  ); }
bool compareOnYear   (Song song1, Song song2) { return  (song1.GetYear()   < song2.GetYear()   ); }
bool compareOnLength (Song song1, Song song2) { return  (song1.GetLength() < song2.GetLength() ); }
bool compareOnSize   (Song song1, Song song2) { return  (song1.GetSize()   < song2.GetSize()   ); }

Collection::Collection() :MediaFile::MediaFile() {};

Collection::Collection(std::string name, std::string author, int length, int size, std::vector <Song> vectorSongs)
{
	MediaFile::SetAll(name, author, length, size);
	Collection::SetVectorSongs(vectorSongs);
}

void Collection::SortVector(std::vector<Song> songVec, std::string key)
{
	if (key == "name")  std::sort(songVec.begin(), songVec.end(), compareOnName);
		if (key == "author") std::sort(songVec.begin(), songVec.end(), compareOnAuthor);
			if (key == "album") std::sort(songVec.begin(), songVec.end(), compareOnAlbum);
				if (key == "ganre") std::sort(songVec.begin(), songVec.end(), compareOnGanre);
					if (key == "year") std::sort(songVec.begin(), songVec.end(), compareOnYear);
						if (key == "length") std::sort(songVec.begin(), songVec.end(), compareOnLength);
							if (key == "size") std::sort(songVec.begin(), songVec.end(), compareOnSize);
}

void printT(std::vector <Song*> vectorSongs)
{
	View::CoutListSongs(vectorSongs);	
}

void Collection::SetVectorSongs(std::vector<Song> song_tmp){ Collection::songVector = song_tmp;}


Collection::~Collection()
{
}
/*
void Collection::AddSong(Song * song)
{
Collection::songCollection.push_back(* song);
}
*/